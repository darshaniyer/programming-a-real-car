## System integration

The building blocks of an autonomous vehicle (AV) stack are one thing, but we have to put them together into one system, and actually control a self-driving car end-to-end. The Udacity self-driving car is called *Carla*, and has four major subsystems depicted in Figure 1:
- The sensor subsystem consists of the hardware components that gather data about the environment. It includes lidar, radar, cameras, and global positioning system (GPS) sensors mounted on the car. 
- The perception subsystem consists of software modules used to process sensor data into structured information that can eventually be used for path planning and control. This is where most of the vehicle analysis of the environment takes place, and hence, is the vehicle center of understanding about its environment. Broadly speaking, it can be divided into two underlying subsystems: 
    - *detection*, which is responsible for understanding the surrounding environment, and includes lane detection, traffic sign and traffic light detection and classification, object detection and tracking, and free space detection. The detection component would most likely use sensor input from camera, lidar, and radar.  
    - *localization*, which is responsible for using sensor and map data to determine the vehicle's precise location. Each component of the perception subsystem relies on a different group of sensors. The localization component would rely on GPS, lidar, and map data, although all of the above mentioned sensors could be used for localization.  
- The planning subsystem uses output from perception subsystem for behavior planning, and also for both short and long range path plan. The planning subsystem determines what maneuver the AV should undertake next, and consists of the following components: 
    - *route planning*, which is a high level path of the vehicle between two points on a map, e.g., roads, highways or freeways 
    - *prediction*, which identifies which maneuver other objects on the road might take in the future, and thus, estimates the future trajectory of other vehicles
    - *behavior planning*, which decides what maneuver our vehicle should take at any point in time, e.g., stopping at traffic light or intersection, changing lanes, accelerating, or making a left turn onto a new street 
    - *trajectory generation*, which plots the precise path the AV would follow, and thus, helps decide the best trajectory for executing a desired immediate behavior.
- The control subsystem ensures that the vehicle follows the path provided by the planning subsystem, and sends actuation commands to the AV. It may include components such as proportional integral differential (PID) controllers, motion predictive control (MPC) or other controllers. It sends throttle, braking, and steering commands to the vehicle. This completes the flow of information from sensors to actuation, and allows the vehicle to drive.

[image1]: ./figures/AV_subsystems.jpg "AV subystems"
![alt text][image1]
**Figure 1 AV subystems: different modules and flow of data**

The goal of this capstone project is to enable *Carla* to drive around Udacity test track using waypoint navigation, while stopping at red traffic lights. Waypoints are simply an ordered set of coordinates that *Carla* uses to plan a path around the track. Each of these waypoints has as an associated target velocity. *Carla's* planning subsystem updates the target velocity for the waypoints ahead of the vehicle, depending on the desired vehicle behavior. *Carla's* control subsystem actuates the throttle, steering, and brake to successfully navigate the waypoints with the correct target velocity.

## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-Capstone).

The simulator for this project can be downloaded from [here](https://github.com/udacity/CarND-Capstone/releases).

The datasets for the traffic light detection and classification can be downloaded from 
1. [Vatsal dataset](https://drive.google.com/file/d/0B-Eiyn-CUQtxdUZWMkFfQzdObUE/view)
2. [Alexander Lechner dataset](https://www.dropbox.com/s/vaniv8eqna89r20/alex-lechner-udacity-traffic-light-dataset.zip?dl=0)
3. [Udacity test area ROS bag](https://s3-us-west-1.amazonaws.com/udacity-selfdrivingcar/traffic_light_bag_file.zip)

## Contents

Folder **workspace** refers to the downloaded Udacity workspace that was used to develop ROS-based software for this project.

Folder **utilities** contains the following helper functions:

* _bag_to_images.py_: python script to extract images from Udacity test area ROS bag
* _create_tf_record.py_: python script to create TF record from training and validation datasets for the tensorFlow Object Detection API
* _train.py_: python script for producing a trained model
* _export_inference_graph.py_: python script for producing a frozen graph for inference from a trained model.

The folder also contains text files to demostrate respective usage for each of the above scripts. 

## Final checks

1. We checked that the AV does not exceed the speed limit of 40 km/h. We verified this by changing the *velocity* rosparam in *waypoint_loader.launch* file to different values and study how the AV responds. We set it to a value of 38 km/h to be on a safer side.
2. We removed the dependence on */vehicle/traffic_lights* topic to get traffic light positions in *tl_detector.py*. This will ensure the code will work without glitch on the test area.
3. We created frozen graphs for the model using TensorFlow version 1.4, which is compatible with environment on the Udacity workspace and *Carla* that has TensorFlow version 1.3. We checked loading of models, passing of images, and subsequent inference on the workspace. It should be fine in *Carla* too.
4. We use *is_site* flag from */traffic_light_config* to load correct model for the simulator Vs test area.
5. We set the *queue_size* to 2 for every publisher to be on a safer side with asynchronous publishing.
6. We even recreated a new model for the test area to deal with different lightning conditions.
7. We made sure that the car stops at the red lights. To be on a safer side, we made the car stop at yellow lights too.
8. We stop and restart PID controllers depending on the state of the boolean message on */vehicle/dbw_enabled* topic.
9. The *dbw_node* publishes throttle, steering, and brake commands at 50Hz.

## Solution video

Please find the video of the simulator for the first few minutes.

![](./videos/Capstone_project_video.mp4)

## Detailed writeup

Detailed report can be found in [_System_integration_writeup.md_](System_integration_writeup.md).

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Though I worked alone on the project due to timing issues, I owe special thanks to the following, without whose help I would not have been able to complete the project on time:
- Steven and Aaron for helpful walkthroughs
- Special thanks to [Alexander Lechner](https://github.com/alex-lechner/Traffic-Light-Classification) for 
    - helpful article on traffic light detection and classification using TensorFlow
    - making traffic light raw datasets available (along with [Vatsal Srivastava](https://github.com/coldKnight/TrafficLight_Detection-TensorFlowAPI#get-the-dataset))
    - inspiration for the state machine in the waypoint updater node
- Udacity for providing test area data in a rosbag
- Knowledge and Student Hub sections of Udacity for valuable discussions. 

