#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseStamped, TwistStamped
from styx_msgs.msg import Lane, Waypoint
from std_msgs.msg import Int32

import math
import numpy as np
from scipy.spatial import KDTree

'''
# We have some base waypoints. We are publishing a lane object that has information including a 
list of waypoints that the car will be following.

# We want to use the current position of the car, which come in over the current_pose topic, 
and then publish the waypoints that are ahead of the car, which will be the final waypoints.

# The final waypoints we will be publishing goes to the waypoint follower, which is a piece of 
code from Autoware, which is running at ~30 Hz

# Once we finish the code, we should be able to start ROS, open up a new terminal, 
source devel/setup.bash, rostopic echo /final_waypoints, boot up the simulator and see if you 
can see the green orbs floating around the car.
'''

'''
This node will publish waypoints from the car's current position to some `x` distance ahead.

As mentioned in the doc, you should ideally first implement a version which does not care
about traffic lights or obstacles.

Once you have created dbw_node, you will update this node to use the status of traffic lights too.

Please note that our simulator also provides the exact location of traffic lights and their
current status in `/vehicle/traffic_lights` message. You can use this message to build this node
as well as to verify your TL classifier.
'''
LOOKAHEAD_WPS = 50 # Number of waypoints we will publish.
DRIVE = 1
STOP = 0

class WaypointUpdater(object):
    def __init__(self):
        
        # Member variables
        self.waypoints_2d = None # This must be initialized before being used in the callback
        self.pose = None
        self.waypoint_tree = None
        self.base_waypoints = None
        self.stopline_wp_index = -1
        self.deceleration_rate = None
        self.acceleration_rate = 0.8
        self.vehicle_velocity = None # in m/s
        self.drive_state = DRIVE
        self.previous_velocity = None

        # Initialize the node
        rospy.init_node('waypoint_updater', log_level=rospy.DEBUG)
 
        # Subscribers
        rospy.Subscriber('/current_pose', PoseStamped, self.pose_cb)
        rospy.Subscriber('/base_waypoints', Lane, self.waypoints_cb)
        rospy.Subscriber('/current_velocity', TwistStamped, self.velocity_cb)        
        rospy.Subscriber('/traffic_waypoint', Int32, self.traffic_cb)

        # Publishers
        self.final_waypoints_pub = rospy.Publisher('/final_waypoints', Lane, queue_size=2)

        self.loop() # this gives us control over the publishing frequency

    def loop(self): # this gives us control over the publishing frequency
        rate = rospy.Rate(10)  
        while not rospy.is_shutdown():
            if self.pose and self.base_waypoints: # once we have information from these two messages
                self.publish_waypoints() # publish a list of waypoints to the /final_waypoints topic
            rate.sleep()
      
    def publish_waypoints(self):
        final_lane = self.generate_lane()        
        self.final_waypoints_pub.publish(final_lane)
        
    # we take waypoints, and update their velocities based on how we want the car to behave.
    # So, if we have the traffic light coming in, we want to slow the car down
    # basically, we adjust the velocity property (waypoint.twist.twist.linear.x) of waypoint, and 
    # then the extra logic in the autoware that brings the car to that speed
    def generate_lane(self):
        lane = Lane() # because the message type is lane, we create a new lane object
        lane.header = self.base_waypoints.header

        closest_idx = self.get_closest_waypoint_idx()
        farthest_idx = closest_idx + LOOKAHEAD_WPS
        base_waypoints = self.base_waypoints.waypoints[closest_idx:farthest_idx]
#       rospy.logwarn("farthest_idx: {0}".format(farthest_idx))
        lane.waypoints = base_waypoints 
        if self.stopline_wp_index != -1 and self.stopline_wp_index > closest_idx and self.stopline_wp_index < farthest_idx:
            lane.waypoints = self.decelerate(base_waypoints, closest_idx)
            self.drive_state = STOP
        elif self.drive_state == STOP:
            self.deceleration_rate = None
            self.drive_state = DRIVE            

        if self.drive_state == DRIVE:
            if abs(self.vehicle_velocity - self.get_waypoint_velocity(lane.waypoints[0])) > 1.0:
                if self.previous_velocity == None:
                    start_velocity = self.vehicle_velocity
                else:
                    start_velocity = max(self.previous_velocity+0.2, self.vehicle_velocity)
                    lane.waypoints = self.accelerate(base_waypoints, start_velocity)
        
        self.previous_velocity = self.vehicle_velocity

        return lane

    def accelerate(self, waypoints, start_velocity):
        temp = []
        for i, waypoint in enumerate(waypoints):
            p = Waypoint()
            p.pose = waypoint.pose

            distance = self.distance(waypoints, 0, i)
            vel = start_velocity + distance * self.acceleration_rate
            if vel < 0.5:
                vel = 0.5

            vel = min(vel, self.get_waypoint_velocity(waypoint))
            p.twist.twist.linear.x = vel
            temp.append(p)

        return temp
    
    def decelerate(self, waypoints, closest_idx):        
        # Three waypoints back from the line so that the car stops at line
        # without -3, we will the stop at the line with the center of the car on the line
        stop_idx = max(self.stopline_wp_index - closest_idx - 3, 0) 
        temp = []
#       rospy.logwarn("N waypoints: {0}".format(len(waypoints)))
        for i, wp in enumerate(waypoints):
            p = Waypoint()
            p.pose = wp.pose # we are not changing the position and orientation of the waypoint, only its velocity    
            dist = self.distance(waypoints, i, stop_idx)
            # larger the distance => larger the velocity
            # => once the distance gets very small, velocity approaches smaller values
            # thus, as we get closer to the stop index, the velocity decreases
            if (i > stop_idx):
                vel = 0.0
            elif (dist < 25.0):                
                if self.deceleration_rate == None:
                    self.deceleration_rate = 0.0
                    if (dist > 0.0):
                        self.deceleration_rate = self.vehicle_velocity / dist
                vel = self.deceleration_rate * dist
                if vel < 1.0: # if the velocity is small enough, we just return 0
                    vel = 0.0
            
#             rospy.logwarn("closest_idx: {0}".format(closest_idx))
#             rospy.logwarn("stopline_wp_index: {0}".format(self.stopline_wp_index))
#             rospy.logwarn("dist: {0}".format(dist))
#             rospy.logwarn("vel: {0}".format(vel))
#             rospy.logwarn("wp vel:{0}".format(wp.twist.twist.linear.x))
                vel = min(vel, self.get_waypoint_velocity(wp))
            else:
                vel = self.get_waypoint_velocity(wp)

            p.twist.twist.linear.x = vel
            temp.append(p)
        return temp	

    def velocity_cb(self, velocity):
        self.vehicle_velocity = velocity.twist.linear.x
    
    def pose_cb(self, msg): 
        self.pose = msg

    def waypoints_cb(self, waypoints):
        self.base_waypoints = waypoints # store waypoints in the object
        # Because this is a latched subscriber as the code is setup in the repo, this copy will not happen every time
        # Once the callback is called, it does not send the base waypoints anymore
        # The idea is we want to take a chunk of these waypoints and use the first 200 in front of the car as a reference
        # KDTree from scipy is used to find the waypoint closest to the car
        # KDTree is a data structure that will help you look up the closest point in the space very efficiently (logn instead of n)
        if  not self.waypoints_2d:
            # we convert the waypoints to just the 2d coordinates for each waypoint
            self.waypoints_2d = [[waypoint.pose.pose.position.x, waypoint.pose.pose.position.y] for waypoint in waypoints.waypoints]
            self.waypoint_tree = KDTree(self.waypoints_2d)

    def traffic_cb(self, msg):
        self.stopline_wp_index = msg.data
#         rospy.logwarn("stopline_wp_index: {0}".format(self.stopline_wp_index))

    def obstacle_cb(self, msg):
        # TODO: Callback for /obstacle_waypoint message. We will implement it later
        pass

    def get_waypoint_velocity(self, waypoint):
        return waypoint.twist.twist.linear.x

    def set_waypoint_velocity(self, waypoints, waypoint, velocity):
        waypoints[waypoint].twist.twist.linear.x = velocity

    def distance(self, waypoints, wp1, wp2):
        dist = 0
        dl = lambda a, b: math.sqrt((a.x-b.x)**2 + (a.y-b.y)**2  + (a.z-b.z)**2)
        for i in range(wp1, wp2+1):
            dist += dl(waypoints[wp1].pose.pose.position, waypoints[i].pose.pose.position)
            wp1 = i
        return dist    
   
    def get_closest_waypoint_idx(self):
        x = self.pose.pose.position.x 
        y = self.pose.pose.position.y
        closest_idx = self.waypoint_tree.query([x,y],1)[1] # query returns position [0] and index [1]

        # check if the closest_idx is ahead or behind the vehicle
        closest_coord = self.waypoints_2d[closest_idx]
        prev_coord = self.waypoints_2d[closest_idx-1]

        # Equation for hyperplane through the closest_coords
        cl_vect = np.array(closest_coord)
        prev_vect = np.array(prev_coord)
        pose_vect = np.array([x,y])

        val = np.dot(cl_vect - prev_vect, pose_vect - cl_vect) # dot prod positive => vectors pointing in the same direction
        if (val>0):
            closest_idx = (closest_idx + 1) % len(self.waypoints_2d)

        return closest_idx

if __name__ == '__main__':
    try:
        WaypointUpdater()
    except rospy.ROSInterruptException:
        rospy.logerr('Could not start waypoint updater node.')