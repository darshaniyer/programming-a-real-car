import rospy
from yaw_controller import *
from pid import *
from lowpass import *

GAS_DENSITY = 2.858
ONE_MPH = 0.44704


''' 
There are lots of ways you can use controller to control the velocity of the car.
You are provided with the yaw_controller which will provide the steering commands - 
you need to put in wheelbase steering ratio, 0.1 m/s is the lowest speed of the car
in m/s, max lateral acceleration, and max steering angle

The velocity coming in over the messages is noisy. So we have created lowpass filter
to filter out the high frequency noise in the velocity
'''

class Controller(object):
    # def __init__(self, *args, **kwargs):
    def __init__(self, vehicle_mass, fuel_capacity, brake_deadband, decel_limit, accel_limit, 
                 wheel_radius, wheel_base, steer_ratio, max_lat_accel, max_steer_angle):
 
        min_speed = 0.1 # lowest speed of the car in m/s
        # Setup the yaw controller
        self.yaw_controller = YawController(wheel_base, steer_ratio, min_speed, max_lat_accel, max_steer_angle)

        kp = 0.8
        ki = 0.1
        kd = 0.2        
        mn = 0.  # Minimum throttle value
        mx = 0.305 # Maximum throttle value
        # Setup the PID controller for throttle
        self.throttle_controller = PID(kp, ki, kd, mn=mn, mx=mx)

        tau = 0.5 # 1/(2*pi*tau) = cutoff frequency
        ts = .02 # sample time
        # Setup the lowpass filter
        self.vel_lpf = LowPassFilter(tau, ts)

        self.vehicle_mass = vehicle_mass 
        self.wheel_radius = wheel_radius
        self.fuel_capacity = fuel_capacity
        self.brake_deadband = brake_deadband 
        self.decel_limit = decel_limit # comfort parameter
        self.accel_limit = accel_limit # comfort parameter
        
        self.last_time = rospy.get_time()
        
        self.last_vel = 0

    # control() is called at 50Hz in the dbw_node
    # dbw_enabled is checked so that we can turn the DBW on and off in the car
        # if we are sitting waiting for traffic light, we might DBW off
        # if we are fixing something, DBW might be off while the code is running
        # if we are using a PID controller that has integral term, then if we do not
        # turn the PID controller off, then we will be accumulating error. This will be bad
        # because if we turn the DBW node back on, there will be accumulated error and the car
        # might do something erratic

    def control(self, current_vel, dbw_enabled, linear_vel, angular_vel):

        if not dbw_enabled:
            self.throttle_controller.reset()
            return 0., 0., 0.       

        # rospy.logwarn("Angular vel: {0}".format(angular_vel))
        # rospy.logwarn("Target vel: {0}".format(linear_vel))
        # rospy.logwarn("Target angular vel: {0}\n".format(angular_vel))
        # rospy.logwarn("Current vel: {0}".format(current_vel))
        # rospy.logwarn("Filtered vel: {0}".format(self.vel_lpf.get()))

        # DI
        # if the car sways a lot, you might want to dampen the angular velocity
        # you will take the difference between the target and current angular velocity
        # if they are the same, then you won't change the steering
        # if they are very different, then you might add some dampening - lowpass filter angular velocity
        
        # Lowpass filter the current velocity value
        current_vel_filt = self.vel_lpf.filt(current_vel)
        
#         rospy.logwarn("Speed limit: {0}".format(self.speed_limit))
#         rospy.logwarn("Current vel: {0}".format(current_vel))
#         rospy.logwarn("Filtered vel: {0}".format(current_vel_filt))      
        
        # Steering control
        steering = self.yaw_controller.get_steering(linear_vel, angular_vel, current_vel_filt)

        vel_error = linear_vel - current_vel_filt
        self.last_vel = current_vel_filt
        
        current_time = rospy.get_time()
        sample_time = current_time - self.last_time
        self.last_time = current_time

        # Throttle control
        throttle = min(self.throttle_controller.step(vel_error, sample_time), 0.305)

        # Brake control
        brake = 0
        if linear_vel == 0.0 and current_vel_filt < 0.1: # we are going very slow => we should probably be trying to stop
            throttle = 0.0
            brake = 700.0 # to stop the car in place when we stop at the light

        elif throttle < 0.1 and vel_error < 0: # we are going faster than our target velocity,
                                # PID is letting up on the throttle we want to slow down
            throttle = 0.0
            decel = max(vel_error, self.decel_limit)
            brake = abs(decel) * (self.vehicle_mass + self.fuel_capacity * GAS_DENSITY) * self.wheel_radius # Torque N-m
            
        brake = min(brake, 700.0)

        return throttle, brake, steering
    

