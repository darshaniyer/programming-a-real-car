#!/usr/bin/env python
import rospy
from std_msgs.msg import Int32
from geometry_msgs.msg import PoseStamped, Pose
from styx_msgs.msg import TrafficLightArray, TrafficLight
from styx_msgs.msg import Lane
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from light_classification.tl_classifier import TLClassifier
import tf
import cv2
import yaml
import numpy as np
from scipy.spatial import KDTree
import os

STATE_COUNT_THRESHOLD = 2

MODEL_SIMULATOR = 'light_classification/models/simulator_ssd_mobilnet.pb'
MODEL_TEST_AREA = 'light_classification/models/test_area_ssd_mobilnet.pb'


class TLDetector(object):
    def __init__(self):       

        # Initialize member variables
        self.waypoints_2d = None # This must be initialized before being used in the callback
        self.pose = None
        self.waypoints = None
        self.camera_image = None
        self.has_image = False
        self.image_ctr = 0
        
        self.state = TrafficLight.UNKNOWN
        self.last_state = TrafficLight.UNKNOWN
        self.last_wp = -1
        self.state_count = 0

        self.stored_line_wp_index = -1 
        self.stored_state = TrafficLight.UNKNOWN
        
        # Initialize the node
        rospy.init_node('tl_detector', log_level=rospy.DEBUG)
        
        # Subscribers
        sub1 = rospy.Subscriber('/current_pose', PoseStamped, self.pose_cb)
        sub2 = rospy.Subscriber('/base_waypoints', Lane, self.waypoints_cb)
        sub6 = rospy.Subscriber('/image_color', Image, self.image_cb) # message contains camera image 

        # The permanent (x, y) coordinates for each traffic light's stop line are provided by the config dictionary, 
        # which is imported from the traffic_light_config file:
        config_string = rospy.get_param("/traffic_light_config")
        self.config = yaml.load(config_string)
        
        model_dir = MODEL_SIMULATOR
        if self.config['is_site']:
            model_dir = MODEL_TEST_AREA       
        
        print('Site flag: ', self.config['is_site'])
        print('MODEL DIR: ', model_dir)
        
        self.bridge = CvBridge()
        self.light_classifier = TLClassifier(model_dir)
        self.listener = tf.TransformListener()        
        
        # Publishers
        self.upcoming_red_light_pub = rospy.Publisher('/traffic_waypoint', Int32, queue_size=2)

        self.loop()

        
    def loop(self): # this gives us control over the publishing frequency
        rate = rospy.Rate(10)   
        while not rospy.is_shutdown():
            if  self.pose and self.waypoints and self.has_image and self.camera_image: 
                self.publish_traffic_waypoint() # publish the red light waypoint index to the /traffic_waypoint topic
            rate.sleep()

            
    def publish_traffic_waypoint(self):
        
        light_wp, state = self.process_traffic_lights() # outputs light waypoint and state

        '''
        Publish upcoming red lights at camera frequency.
        Each predicted state has to occur `STATE_COUNT_THRESHOLD` number
        of times till we start using it. Otherwise the previous stable state is
        used.
        State is the state of the traffic lights based on the processing of the traffic lights.
        We store the previous state of the traffic light. Every time an image comes into this callback, 
        we process that image with the traffic light and determine 
        if the state has changed from green to yellow or yellow to red => gone through some transition.
        If so, we setup the counter to make sure that the light is staying consistent before we take any action as a
        protection against classifier noise
        '''
        if self.state != state:
            self.state_count = 0
            self.state = state
            self.image_ctr = 0
            self.stored_line_wp_index = light_wp
            self.stored_state = state
        elif self.state_count >= STATE_COUNT_THRESHOLD:
            self.last_state = self.state
            if state == TrafficLight.RED or state == TrafficLight.YELLOW:
                light_wp = light_wp  
            else:
                light_wp = -1
            self.last_wp = light_wp
            self.upcoming_red_light_pub.publish(Int32(light_wp))
        else:
            self.upcoming_red_light_pub.publish(Int32(self.last_wp))
        self.state_count += 1
        
#         rospy.logwarn("self.state: {0}".format(self.state))
#         rospy.logwarn("state: {0}".format(state))
#         rospy.logwarn("last_wp: {0}".format(self.last_wp))
#         rospy.logwarn("light_wp: {0}".format(light_wp))
#         rospy.logwarn("state_count: {0}".format(self.state_count))


    def pose_cb(self, msg):
        self.pose = msg

    def waypoints_cb(self, waypoints):
        self.waypoints = waypoints

        if not self.waypoints_2d:
            # we convert the waypoints to just the 2d coordinates for each waypoint
            self.waypoints_2d = [[waypoint.pose.pose.position.x, waypoint.pose.pose.position.y] for waypoint in waypoints.waypoints]
            self.waypoint_tree = KDTree(self.waypoints_2d)

            
    def image_cb(self, msg):
        """Identifies red lights in the incoming camera image and publishes the index
            of the waypoint closest to the red light's stop line to /traffic_waypoint

        Args:
            msg (Image): image from car-mounted camera

        """
        self.has_image = True
        self.camera_image = msg
#       rospy.logwarn("has_image: {0}".format(self.has_image))


    def get_closest_waypoint(self, x, y):
        """Identifies the closest path waypoint to the given position
            https://en.wikipedia.org/wiki/Closest_pair_of_points_problem
        Args:
            pose (Pose): position to match a waypoint to

        Returns:
            int: index of the closest waypoint in self.waypoints

        """
        closest_idx = self.waypoint_tree.query([x,y],1)[1] # query returns position [0] and index [1]

        return closest_idx

    
    def get_light_state(self):
        """Determines the current color of the traffic light

        Returns:
            int: ID of traffic light color (specified in styx_msgs/TrafficLight)

        """
        if (not self.has_image):
            self.prev_light_loc = None
            return TrafficLight.UNKNOWN

        cv_image = self.bridge.imgmsg_to_cv2(self.camera_image, "rgb8")

        classifier_output = self.light_classifier.get_classification(cv_image)
#       rospy.logwarn("classifier_output: {0}".format(classifier_output))
        
        return classifier_output

    
    def process_traffic_lights(self):
        """Finds closest visible traffic light, if one exists, and determines its
            location and color

        Returns:
            int: index of waypoint closes to the upcoming stop line for a traffic light (-1 if none exists)
            int: ID of traffic light color (specified in styx_msgs/TrafficLight)

        """
        light_loc = None
        closest_light = None
        line_wp_index = -1 
        state = TrafficLight.UNKNOWN

        # List of positions that correspond to the line to stop in front of for a given intersection
        stop_line_positions = self.config['stop_line_positions']
        if (self.pose):
            car_wp_index = self.get_closest_waypoint(self.pose.pose.position.x, self.pose.pose.position.y)

            # Find the closest visible traffic light (if one exists)
            diff = len(self.waypoints.waypoints)
            self.image_ctr += 1           
            for i, light_loc in enumerate(stop_line_positions): # there are 8 lights
                # Get the stop line waypoint index
                line = stop_line_positions[i]
                temp_wp_index = self.get_closest_waypoint(line[0], line[1])

                # Find the line waypoint index closest to the car waypoint index
                d = temp_wp_index - car_wp_index
                if (d >= 0 and d < diff): # d >=0 means in front of the car
                    diff = d
                    closest_light = light_loc
                    line_wp_index = temp_wp_index
#                   rospy.logwarn("car_wp_index: {0}".format(car_wp_index))
#                   rospy.logwarn("line_wp_index: {0}".format(temp_wp_index))
#                   rospy.logwarn("d: {0}".format(d))                    
            
            d = line_wp_index-car_wp_index
            if closest_light and d <= 300:
                if self.image_ctr >= 5: 
                    state = self.get_light_state()
                    self.image_ctr = 0
                    self.stored_line_wp_index = line_wp_index 
                    self.stored_state = state
                else:
                    line_wp_index = self.stored_line_wp_index 
                    state = self.stored_state    
            else:
                line_wp_index = -1 
                state = TrafficLight.UNKNOWN
                self.stored_line_wp_index = -1
                self.stored_state = TrafficLight.UNKNOWN
                
#       rospy.logwarn("d: {0}".format(d))              
#       rospy.logwarn("image_ctr: {0}".format(self.image_ctr))
#       rospy.logwarn("state: {0}".format(state))   
#       rospy.logwarn("line_wp_index: {0}".format(line_wp_index))        
#       rospy.logwarn("stored_line_wp_index: {0}".format(self.stored_line_wp_index))   
#       rospy.logwarn("stored_state: {0}".format(self.stored_state))        
        
        return line_wp_index, state

    
    def distance(self, waypoints, wp1, wp2):
        dist = 0
        dl = lambda a, b: math.sqrt((a.x-b.x)**2 + (a.y-b.y)**2  + (a.z-b.z)**2)
        for i in range(wp1, wp2+1):
            dist += dl(waypoints[wp1].pose.pose.position, waypoints[i].pose.pose.position)
            wp1 = i
            
        return dist

    
if __name__ == '__main__':
    try:
        TLDetector()
    except rospy.ROSInterruptException:
        rospy.logerr('Could not start traffic node.')