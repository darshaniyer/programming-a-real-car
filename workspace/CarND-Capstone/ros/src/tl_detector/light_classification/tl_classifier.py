import numpy as np
import tensorflow as tf
import datetime
from styx_msgs.msg import TrafficLight

class TLClassifier(object):
    def __init__(self, model_path):

        self.threshold = .5 # Confidence threshold

        self.detection_graph = self.load_graph(model_path)

        # `get_tensor_by_name` returns the Tensor with the associated name in the Graph.
        self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')

        # Each score represent the level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')

        # The classification of the object (integer id).
        self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')

        # As a safegaurd againt GPU memory issues
        self.config = tf.ConfigProto()
        self.config.gpu_options.allow_growth = True        
        self.sess = tf.Session(config=self.config, graph=self.detection_graph)
        
        # Warm up for the classifier
        start = datetime.datetime.now()
        gen_image = np.uint8(np.random.randn(1, 1096, 1368, 3))
        classes, scores = self.sess.run([self.detection_classes, self.detection_scores], feed_dict={self.image_tensor: gen_image})   
        end = datetime.datetime.now()
        c = end - start
        print('WARMUP INFERENCE TIME (ms):', c.total_seconds()*1000.0)
        

    def get_classification(self, image):
        """Determines the color of the traffic light in the image

        Args:
            image (cv::Mat): image containing the traffic light

        Returns:
            int: ID of traffic light color (specified in styx_msgs/TrafficLight)

        """       
        start = datetime.datetime.now()
        # Load a sample image.
        image_np = np.expand_dims(np.asarray(image, dtype=np.uint8), 0)
        
        output_class = TrafficLight.UNKNOWN
        # Actual detection.
        classes, scores = self.sess.run([self.detection_classes, self.detection_scores], feed_dict={self.image_tensor: image_np})
        
        # Remove unnecessary dimensions
        classes = np.squeeze(classes).astype(np.int32)
        scores = np.squeeze(scores)

        print('SCORES: ', scores[0])
        print('CLASSES: ', classes[0])

        output_class = self.map_classifier_output(classes[0], scores[0], self.threshold)     
        
        end = datetime.datetime.now()
        c = end - start
        print('INFERENCE TIME (ms):', c.total_seconds()*1000.0)

        return output_class
    
    
    def map_classifier_output(self, prediction, score, thresh):

        mapped_class = TrafficLight.UNKNOWN
        if score > thresh:        
            if prediction == 1:
                mapped_class = TrafficLight.GREEN
                print('GREEN')
            elif prediction == 2:
                mapped_class = TrafficLight.RED
                print('RED')
            elif prediction == 3:
                mapped_class = TrafficLight.YELLOW
                print('YELLOW')

        return mapped_class
    

    def load_graph(self, graph_file):
        """Loads a frozen inference graph"""
        graph = tf.Graph()
        with graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(graph_file, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
        return graph

