python export_inference_graph.py --input_type image_tensor --pipeline_config_path config/ssd_inception_v2_coco_simulator.config --trained_checkpoint_prefix train_models/ssd_inception/simulator/model.ckpt-20000 --output_directory train_models/ssd_inception/simulator/frozen

python export_inference_graph.py --input_type image_tensor --pipeline_config_path config/ssd_inception_v2_coco_test_area.config --trained_checkpoint_prefix train_models/ssd_inception/test_area/model.ckpt-20000 --output_directory train_models/ssd_inception/test_area/frozen

python export_inference_graph.py --input_type image_tensor --pipeline_config_path config/ssd_mobilenet_v1_coco_simulator.config --trained_checkpoint_prefix train_models/ssd_mobilnet/simulator/model.ckpt-20000 --output_directory train_models/ssd_mobilnet/simulator/frozen

python export_inference_graph.py --input_type image_tensor --pipeline_config_path config/ssd_mobilenet_v1_coco_simulator.config --trained_checkpoint_prefix train_models/ssd_mobilnet/test_area/model.ckpt-25000 --output_directory train_models/ssd_mobilnet/test_area/frozen

python export_inference_graph.py --input_type image_tensor --pipeline_config_path config/faster_rcnn_resnet50_coco_simulator.config --trained_checkpoint_prefix train_models/faster_rcnn/simulator/model.ckpt-20000 --output_directory train_models/faster_rcnn/simulator/frozen

python export_inference_graph.py --input_type image_tensor --pipeline_config_path config/faster_rcnn_resnet50_coco_test_area.config --trained_checkpoint_prefix train_models/faster_rcnn/test_area/model.ckpt-20000 --output_directory train_models/faster_rcnn/test_area/frozen


# https://stackoverflow.com/questions/48215159/error-exporting-inference-graph-valueerror

# set line 71 in exporter.py to rewrite_options = rewriter_config_pb2.RewriterConfig()

python export_inference_graph.py --input_type image_tensor --pipeline_config_path config/ssd_inception_v2_coco_test_area.config --trained_checkpoint_prefix train_models/ssd_inception/test_area/model.ckpt-25000 --output_directory train_models/ssd_inception/test_area/frozen


