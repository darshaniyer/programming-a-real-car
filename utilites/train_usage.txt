python train.py --logtostderr --train_dir=train_models/ssd_inception/simulator --pipeline_config_path=config/ssd_inception_v2_coco_simulator.config

python train.py --logtostderr --train_dir=train_models/ssd_inception/test_area --pipeline_config_path=config/ssd_inception_v2_coco_test_area.config

python train.py --logtostderr --train_dir=train_models/ssd_mobilnet/simulator --pipeline_config_path=config/ssd_mobilenet_v1_coco_simulator.config

python train.py --logtostderr --train_dir=train_models/ssd_mobilnet/test_area --pipeline_config_path=config/ssd_mobilenet_v1_coco_test_area.config

python train.py --logtostderr --train_dir=train_models/faster_rcnn/simulator --pipeline_config_path=config/faster_rcnn_resnet50_coco_simulator.config

python train.py --logtostderr --train_dir=train_models/faster_rcnn/test_area --pipeline_config_path=config/faster_rcnn_resnet50_coco_test_area.config


python train.py --logtostderr --train_dir=train_models/ssd_mobilnet/simulator/retrain --pipeline_config_path=config/ssd_mobilenet_v1_coco_simulator_retrain.config



python train.py --logtostderr --train_dir=train_models/ssd_inception/test_area --pipeline_config_path=config/ssd_inception_v2_coco_test_area.config

python train.py --logtostderr --train_dir=train_models/ssd_mobilnet/test_area --pipeline_config_path=config/ssd_mobilenet_v1_coco_test_area.config